# -*- coding: utf-8 -*-

"""Testing test4 part1"""

import datetime
import pytest

import requests

from test4 import ArgumentError, ServerError, ServConnectionError, \
    args_checker, check_arrival, date_from_str, flybulgarien_flying, get_departure_list, get_timetable, main, \
    messenger, parse_flight_info, timetable_parser, timetable_printer, total_coast


@pytest.mark.parametrize('xpath_result, expected', [
    (
        [],                             # xpath_result
        []                              # expected
    ),
    (
        ['aaa', 'qwerty'],              # xpath_result
        ['aaa', 'qwerty']               # expected
    ),
    (
        ['AAA', '123', '', 'DEF', ''],  # xpath_result
        ['AAA', '123', 'DEF']           # expected
    )
])
def test_get_departure_list_return_value(mocker, xpath_result, expected):
    response = mocker.Mock(status_code=requests.codes.ok)
    mocker.patch('test4.requests.get', return_value=response)
    mocker.patch('test4.html').fromstring.return_value.xpath.return_value = xpath_result
    assert get_departure_list() == expected


def test_get_departure_list_raise_ServConnectionError(mocker):
    response = mocker.Mock(status_code='Fail')
    mocker.patch('test4.requests.get', return_value=response)
    mocker.patch('test4.html')
    with pytest.raises(ServConnectionError):
        get_departure_list()


@pytest.mark.parametrize('data, date_now, departure_list', [
    (
        {                                               # data
            'departure': 'ASD', 'arrival': 'BCV',
            'departure_date': datetime.date(2019, 7, 3), 'return_date': datetime.date(2019, 7, 3)
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ASD', 'QWE']                                  # departure_list
    ),
    (
        {                                               # data
            'departure': 'ASD', 'arrival': 'BCV',
            'departure_date': datetime.date(2019, 7, 13), 'return_date': None
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ASD', 'QWE']                                  # departure_list
    )
])
def test_args_checker_return_value(mocker, data, date_now, departure_list):
    mocker.patch('test4.datetime').now.return_value.date.return_value = date_now
    mocker.patch('test4.get_departure_list', return_value=departure_list)
    assert args_checker(**data) == data


@pytest.mark.parametrize('input_data, date_now, departure_list', [
    # Error airport departure: IATA code is wrong or there are no departures from the airport
    (
        {                                               # input_data
            'departure': 'ASD', 'arrival': 'BCV',
            'departure_date': datetime.date(2019, 7, 13), 'return_date': datetime.date(2019, 8, 13)
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ZXC', 'QWE']                                  # departure_list
    ),
    # Error IATA code arrival
    (
        {                                               # input_data
            'departure': 'ASD', 'arrival': 'BC',
            'departure_date': datetime.date(2019, 7, 13), 'return_date': datetime.date(2019, 8, 13)
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ASD', 'QWE']                                  # departure_list
    ),
    # Error IATA code arrival
    (
        {                                               # input_data
            'departure': 'ASD', 'arrival': 'BC8',
            'departure_date': datetime.date(2019, 7, 13), 'return_date': datetime.date(2019, 8, 13)
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ASD', 'QWE']                                  # departure_list
    ),
    # Date of departure before the current date
    (
        {                                               # input_data
            'departure': 'ASD', 'arrival': 'BCZ',
            'departure_date': datetime.date(2019, 7, 2), 'return_date': datetime.date(2019, 8, 13)
        },
        datetime.date(2019, 7, 3),                      # datetime_now
        ['ASD', 'QWE']                                  # departure_list
    ),
    # Return date before departure date
    (
        {                                               # input_data
            'departure': 'ASD', 'arrival': 'BCZ',
            'departure_date': datetime.date(2019, 7, 5), 'return_date': datetime.date(2019, 7, 4)
        },
        datetime.date(2019, 7, 3),                      # date_now
        ['ASD', 'QWE']                                  # departure_list
    ),
])
def test_args_checker_raise_ArgumentError(mocker, input_data, date_now, departure_list):
    mocker.patch('test4.datetime').now.return_value.date.return_value = date_now
    mocker.patch('test4.get_departure_list', return_value=departure_list)
    with pytest.raises(ArgumentError):
        args_checker(**input_data)


@pytest.mark.parametrize('str_date, expected', [
    (
        None,                           # str_date
        None                            # expected
    ),
    (
        'aaa',                          # str_date
        'aaa'                           # expected
    )
])
def test_date_from_str_return_value(mocker, str_date, expected):
    mocker.patch('test4.datetime').strptime.return_value.date.return_value = expected
    assert date_from_str(str_date) == expected


@pytest.mark.parametrize('error', [
    ValueError,
    TypeError
])
def test_date_from_str_raise_ArgumentError(mocker, error):
    mocker.patch('test4.datetime').strptime.side_effect = error
    with pytest.raises(ArgumentError):
        date_from_str('h')


@pytest.mark.parametrize('args, mock_list_result, expected', [
    (
        ['aa', 'fake'],     # args
        ['hype', 'fake'],   # mock_list_result
        True                # expected
    ),
    (
        ['aaa', 'qwerty'],  # args
        ['hype', 'fake'],   # mock_list_result
        False               # expected
    )
])
def test_check_arrival_return_value(mocker, args, mock_list_result, expected):
    json_mock = mocker.Mock(return_value=mock_list_result)
    response = mocker.Mock(status_code=requests.codes.ok, json=json_mock)
    mocker.patch('test4.requests.get', return_value=response)
    assert check_arrival(*args) == expected


def test_check_arrival_raise_ServConnectionError(mocker):
    response = mocker.Mock(status_code='Fail')
    mocker.patch('test4.requests.get', return_value=response)
    with pytest.raises(ServConnectionError):
        check_arrival('qwe', 'rty')


@pytest.mark.parametrize('args', [
    ['abc', 'fake', datetime.date(2019, 7, 5), datetime.date(2019, 7, 20), 5],
    ['abc', 'fake', datetime.date(2019, 7, 5)]
])
def test_get_timetable_return_value(mocker, args):
    response = mocker.Mock(status_code=requests.codes.ok)
    mocker.patch('test4.requests.get', return_value=response)
    result = mocker.Mock()
    result.xpath.return_value = 'empty data'
    mocker.patch('test4.html.fromstring', return_value=result)
    assert get_timetable(*args) == result


@pytest.mark.parametrize('status_code, type_error', [
    (
        requests.codes.ok,          # status_code
        ServerError                 # type_error
    ),
    (
        404,                        # status_code
        ServConnectionError         # type_error
    )
])
def test_get_timetable_raise(mocker, status_code, type_error):
    response = mocker.Mock(status_code=status_code)
    mocker.patch('test4.requests.get', return_value=response)
    mocker.patch('test4.html').fromstring.return_value.xpath.return_value = \
        'An internal error occurred. Please retry your request.'
    with pytest.raises(type_error):
        get_timetable('abc', 'fake', mocker.Mock())


@pytest.mark.parametrize('args, xpath_return_value, parse_flight_info_return_value, expected', [
    (
        ['cph', 'boj', 'fake date', 'fake date'],           # args
        ['hype', 'flywiz_rinf', 'fake', 'flywiz_irinf'],    # xpath_return_value
        'qwerty',                                           # parse_flight_info_return_value
        {                                                   # expected
            'departure': 'cph',
            'arrival': 'boj',
            'Going Out': ['qwerty'],
            'Coming Back': ['qwerty']
        }
    ),
    (
        ['cph', 'boj', 'fake date', 'fake date'],           # args
        ['flywiz_rinf', 'flywiz_irinf'],                    # xpath_return_value
        'qwerty',                                           # parse_flight_info_return_value
        {                                                   # expected
            'departure': 'cph',
            'arrival': 'boj',
            'Going Out': ['qwerty'],
            'Coming Back': ['qwerty']
        }
    ),
    (
        ['cph', 'boj', 'fake date', 'fake date'],           # args
        ['hype', 'flywiz_rinf', 'fake', 'flywiz_irinf'],    # xpath_return_value
        None,                                               # parse_flight_info_return_value
        {                                                   # expected
            'departure': 'cph',
            'arrival': 'boj',
            'Going Out':  ['No available flights found.'],
            'Coming Back': ['No available flights found.']
        }
    ),
    (
        ['cph', 'boj', 'fake date', 'fake date'],           # args
        [],                                                 # xpath_return_value
        None,                                               # parse_flight_info_return_value
        {                                                   # expected
            'departure': 'cph',
            'arrival': 'boj',
            'Going Out':  ['No available flights found.'],
            'Coming Back':  ['No available flights found.']
        }
    ),
    (
        ['cph', 'boj', 'fake date', None],                  # args
        ['hype', 'flywiz_irinf', 'flywiz_rinf', 'fake'],    # xpath_return_value
        None,                                               # parse_flight_info_return_value
        {                                                   # expected
            'departure': 'cph',
            'arrival': 'boj',
            'Going Out':  ['No available flights found.']
        }
    )
])
def test_timetable_parser_return_value(mocker, args, xpath_return_value, parse_flight_info_return_value, expected):
    page = mocker.Mock()
    page.xpath.return_value = xpath_return_value
    mocker.patch('test4.parse_flight_info', return_value=parse_flight_info_return_value)
    assert timetable_parser(page, *args) == expected


@pytest.mark.parametrize('arg, expected', [
    # if -> true
    (
        datetime.date(2019, 7, 22),                         # arg
        {                                                   # expected
            'Date': datetime.date(2019, 7, 22),
            'Depart time': datetime.time(18, 45),
            'Arrive time': datetime.time(22, 50),
            'Price': (6.66, 'tugr'), 'Class': 'Economy'
        }
    ),
    # if -> false
    (
        datetime.date(2019, 7, 20),                         # arg
        None                                                # expected
    ),
])
def test_parse_flight_info_return_value(mocker, arg, expected):
    price = mocker.Mock()
    price.replace.return_value.split.return_value = ('100', 'tugr')
    page = mocker.Mock()
    page.xpath.side_effect = [['Mon, 22 Jul 2019'], ['18:45'], ['22:50'], [price]]
    tr_id = mocker.Mock()
    tr_id.replace.return_value = 'blablabla'
    mocker.patch('test4.float', return_value=6.66)
    assert parse_flight_info(tr_id, page, arg) == expected


@pytest.mark.parametrize('timetable, call_count', [
    # if -> true, iterate loop for
    (
        {                                               # timetable
            'departure': 'BLL',
            'arrival': 'BOJ',
            'Going Out': ['data1', 'data2', 'data3'],
            'Coming Back': []
        },
        3                                               # call_count
    ),
    # if -> true, don't iterate loop for
    # Note: we determine that "if -> true" worked according to the previous test
    (
        {                                               # timetable
            'departure': 'BLL',
            'arrival': 'BOJ',
            'Going Out': [],
            'Coming Back': []
        },
        0                                               # call_count
    ),
    # if -> false, iterate loop for
    (
        {                                               # timetable
            'departure': 'BLL',
            'arrival': 'BOJ',
            'Going Out': ['data1', 'data2', 'data3'],
            'Coming Back': ['data1', 'data2', 'data3']
        },
        12                                              # call_count
    ),
    # if -> false, don't iterate loop for
    # Note: we determine that "if -> false" worked according to the previous test
    (
        {                                               # timetable
            'departure': 'BLL',
            'arrival': 'BOJ',
            'Going Out': [],
            'Coming Back': ['data1', 'data2', 'data3']
        },
        0                                               # call_count
    )
])
def test_timetable_printer_return_value(mocker, timetable, call_count):
    messenger = mocker.patch('test4.messenger')
    mocker.patch('test4.print')
    mocker.patch('test4.total_coast')
    timetable_printer(timetable)
    assert messenger.call_count == call_count


@pytest.mark.parametrize('flight, expected', [
    # test block try
    (
        {                               # flight
            'Date': datetime.date(2019, 7, 22),
            'Depart time': datetime.time(18, 45), 'Arrive time': datetime.time(22, 50),
            'Price': (699.0, 'tugr'), 'Class': 'Economy'
        },
        # expected
        'Date: Mon, 22 Jul 2019 | Depart time: 18:45 | Arrive time: 22:50 | Class: Economy | Coast: 699.0 tugr'
    ),
    # test except TypeError
    (
        'blablabla',                    # flight
        'No available flights found.'   # expected
    ),
    # test except KeyError
    (
        {},                             # flight
        'No available flights found.'   # expected
    )
])
def test_messenger_return_value(mocker, flight, expected):
    mocker.patch('test4.datetime').strftime.return_value = 'time'
    assert messenger(flight) == expected


@pytest.mark.parametrize('args, expected', [
    # if -> true, if -> true
    (
        [{'Price': (123.0, 'tugr')}, {'Price': (321.0, 'tugr')}],
        'Total coast: 444.0 tugr'
    ),
    # if -> true, if -> false (if different currency)
    (
        [{'Price': (1.0, 'tugr')}, {'Price': (1000.0, 'baks')}],
        ''
    ),
    # if -> false (one of the values doesn't contain a price)
    (
        ['No available flights found.', {'Price': (1000.0, 'baks')}],
        ''
    ),
])
def test_total_coast_return_value(args, expected):
    assert total_coast(*args) == expected


@pytest.mark.parametrize('state', [
    True,   # if -> true
    False,  # if -> false
])
def test_flybulgarien_flying_if_else(mocker, state):
    mocker.patch('test4.get_timetable')
    mocker.patch('test4.timetable_parser')
    mocker.patch('test4.print')
    mocker.patch('test4.check_arrival', return_value=state)
    timetable_printer = mocker.patch('test4.timetable_printer')
    flybulgarien_flying('a', 'b', 'c', 'd')
    assert timetable_printer.called == state


def test_main_try(mocker):
    std_out = mocker.patch('test4.print')
    mocker.patch('test4.get_arg_params')
    mocker.patch('test4.args_handler')
    mocker.patch('test4.args_checker')
    mocker.patch('test4.flybulgarien_flying', side_effect='ok')
    main()
    std_out.assert_not_called()


@pytest.mark.parametrize('flybulgarien_flying_side_effect, message', [
    (requests.exceptions.ConnectionError, 'Unable to access server.'),
    (requests.exceptions.ReadTimeout, 'Unable to access server.'),
    (requests.exceptions.ConnectTimeout, 'Unable to access server.'),
    (ServConnectionError, 'Unable to access server.'),
    (ServerError, 'An internal error occurred on the server.'),
])
def test_main_except(mocker, flybulgarien_flying_side_effect, message):
    std_out = mocker.patch('test4.print')
    mocker.patch('test4.get_arg_params')
    mocker.patch('test4.args_handler')
    mocker.patch('test4.args_checker')
    mocker.patch('test4.flybulgarien_flying', side_effect=flybulgarien_flying_side_effect)
    main()
    std_out.assert_called_with(message)


def test_main_except_ArgumentError(mocker):
    std_out = mocker.patch('test4.print')
    mocker.patch('test4.get_arg_params')
    mocker.patch('test4.args_handler')
    mocker.patch('test4.args_checker')
    mocker.patch('test4.flybulgarien_flying', side_effect=ArgumentError)
    main()
    std_out.assert_any_call('Error in one of the attributes.')
    assert std_out.call_count == 2


# pytest --cov-report term-missing --cov test4 test_test4.py
