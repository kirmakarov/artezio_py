# -*- coding: utf-8 -*-

"""test4 part1"""

import argparse

from collections import defaultdict
from datetime import datetime

import requests

from lxml import html

DATE_FORMAT = '%a, %d %b %Y'
TIME_FORMAT = '%H:%M'


class ServConnectionError(ConnectionError):
    """Connection error to server."""
    def __str__(self):
        return 'Unsuccessful attempt to connect to a resource'


class ServerError(ConnectionError):
    """Server side error."""
    def __str__(self):
        return 'An internal error has occurred on the server'


class ArgumentError(Exception):
    """Error in start argument."""


def get_departure_list():
    """Return list IATA-code airports departures."""
    response = requests.get('http://www.flybulgarien.dk/en/', timeout=(1, 5))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    page = html.fromstring(response.text)
    departure_cities = [city for city in page.xpath('//select[@id="departure-city"]/option/@value') if city]
    return departure_cities


def get_arg_params():
    """Startup key getting. Get command line arguments and return dict with startup parameters."""
    cmd_parser = argparse.ArgumentParser(description='Display of flight information company Fly Bulgarien')
    cmd_parser.add_argument('-d', dest='departure', required=True, help='IATA-code airport departure')
    cmd_parser.add_argument('-a', dest='arrival', required=True, help='IATA-code airport arrival')
    cmd_parser.add_argument('-date_depart', dest='departure_date_str', required=True,
                            help='Departure date. Format: dd.mm.yyyy')
    cmd_parser.add_argument('-date_return', dest='return_date_str', default='',
                            help='Return flight date. Format: dd.mm.yyyy')
    return vars(cmd_parser.parse_args())


def args_handler(departure, arrival, departure_date_str, return_date_str):
    """
    Сonverts script startup parameters.
    Date string format to object date, srtings arrival and departure to upper register.
    """
    departure = departure.upper()
    arrival = arrival.upper()
    departure_date = date_from_str(departure_date_str, 'departure_date')
    return_date = date_from_str(return_date_str, 'return_date')
    return {
        'departure': departure,
        'arrival': arrival,
        'departure_date': departure_date,
        'return_date': return_date,
    }


def args_checker(departure, arrival, departure_date, return_date):
    """Validates startup parameters."""
    today = datetime.now().date()
    # departure check is one of the airports from where flights are made by the airline
    if departure not in get_departure_list():
        raise ArgumentError('Error airport departure: IATA code is wrong or there are no departures from the airport')
    # Check out the arrival of the IATA code format
    if len(arrival) != 3 or not arrival.isalpha():
        raise ArgumentError('Error IATA code arrival')
    # Direct flight date not earlier than current date
    if departure_date < today:
        raise ArgumentError('Date of departure before the current date')
    # Return flight date not earlier than direct flight date
    if return_date and departure_date > return_date:
        raise ArgumentError('Return date before departure date')
    return {
        'departure': departure,
        'arrival': arrival,
        'departure_date': departure_date,
        'return_date': return_date,
    }


def date_from_str(str_date, name_date=None):
    """Converts date string format to object date. Checks string for formatting."""
    if str_date:
        try:
            return datetime.strptime(str_date, "%d.%m.%Y").date()
        except (TypeError, ValueError):
            raise ArgumentError('Date format "{}" is not correct'.format(name_date))


def check_arrival(departure, arrival):
    """Checking the availability of flights from departure to arrival."""
    url = 'http://www.flybulgarien.dk/script/getcity/2-{}'.format(departure)
    response = requests.get(url, timeout=(1, 5))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    if arrival not in response.json():
        return False
    return True


def get_timetable(departure, arrival, departure_date, return_date=None, passengers=1):
    """Returns a page with a flight schedule between 2 airports on specified dates, and on neighboring."""
    params = {
        'lang': 'en',
        'depdate': departure_date.strftime("%d.%m.%Y"),
        'aptcode1': departure,
        'aptcode2': arrival,
        'paxcount': passengers,
        'infcount': ''
    }
    if return_date:
        params.update({
            'rt': '',
            'rtdate': return_date.strftime("%d.%m.%Y")
        })
    else:
        params['ow'] = ''
    response = requests.get('https://apps.penguin.bg/fly/quote3.aspx', params=params, timeout=(3, 15))
    if response.status_code != requests.codes.ok:
        raise ServConnectionError()
    page = html.fromstring(response.text)
    if 'An internal error occurred. Please retry your request.' in page.xpath('//body/div[@class="cnf"]/text()'):
        raise ServerError()
    return page


def timetable_parser(page, departure, arrival, departure_date, return_date):
    """
    Parses the flight schedule page and forms a dictionary with a flight schedule between 2 airports on the specified
    dates.
    """
    timetable = defaultdict(list)
    timetable['departure'] = departure
    timetable['arrival'] = arrival
    for tr_id in page.xpath('//table[@id="flywiz_tblQuotes"]/tr/@id'):
        if 'flywiz_rinf' in tr_id:
            flight = parse_flight_info(tr_id, page, departure_date)
            if flight:
                timetable['Going Out'].append(flight)
        elif return_date and 'flywiz_irinf' in tr_id:
            flight = parse_flight_info(tr_id, page, return_date)
            if flight:
                timetable['Coming Back'].append(flight)
    # If there are no flights in the direction, add a line with information about no flights.
    if not timetable['Going Out']:
        timetable['Going Out'].append('No available flights found.')
    if return_date and not timetable['Coming Back']:
        timetable['Coming Back'].append('No available flights found.')
    return timetable


def parse_flight_info(tr_id, page, date):
    """Parsing a page with a flight schedule and forms a dictionary with information about a single flight."""
    date_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[2]/text()', name=tr_id)[0]
    option = None
    date_flight = datetime.strptime(date_str, DATE_FORMAT).date()
    if date == date_flight:
        depart_time_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[3]/text()', name=tr_id)[0]
        depart_time = datetime.strptime(depart_time_str, TIME_FORMAT).time()
        arrive_time_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[4]/text()', name=tr_id)[0]
        arrive_time = datetime.strptime(arrive_time_str, TIME_FORMAT).time()
        tag = tr_id.replace('rinf', 'rprc')
        price_str = page.xpath('//table[@id="flywiz_tblQuotes"]/tr[@id=$name]/td[2]/text()', name=tag)[0]
        value, currency = price_str.replace('Price:  ', '').split(' ')
        price = (float(value), currency)
        option = {
            'Date': date_flight, 'Depart time': depart_time,
            'Arrive time': arrive_time, 'Price': price, 'Class': 'Economy'
        }
    return option


def timetable_printer(timetable):
    """Displays flight schedules."""
    out_way = "{} -> {}".format(timetable['departure'], timetable['arrival'])
    back_way = "{} <- {}".format(timetable['departure'], timetable['arrival'])
    if not timetable.get('Coming Back'):
        for num, flight in enumerate(timetable['Going Out'], start=1):
            print('Variant #', num)
            print(out_way, '|', messenger(flight))
            print('-' * 120)
    else:
        for flight in timetable['Going Out']:
            going_out = messenger(flight)
            for num, flight_back in enumerate(timetable['Coming Back'], start=1):
                coming_back = messenger(flight_back)
                print('Variant #', num)
                print(out_way, '|', going_out)
                print(back_way, '|', coming_back)
                print(total_coast(flight, flight_back))
                print('-' * 120)


def messenger(flight):
    """Returns a string containing information about a single flight."""
    try:
        date = flight['Date'].strftime(DATE_FORMAT)
        depart = flight['Depart time'].strftime(TIME_FORMAT)
        arrive = flight['Arrive time'].strftime(TIME_FORMAT)
        price = str(flight['Price'][0]) + ' ' + flight['Price'][1]
        class_fly = flight['Class']
        message = 'Date: {} | Depart time: {} | Arrive time: {} | Class: {} | Coast: {}'.format(date, depart, arrive,
                                                                                                class_fly, price)
    except (TypeError, KeyError):
        message = 'No available flights found.'
    return message


def total_coast(flight, flight_back):
    """Returns the total flight cost string. If there is no data, returns an empty string."""
    if isinstance(flight, dict) and isinstance(flight_back, dict):
        if flight['Price'][1] == flight_back['Price'][1]:
            price_sum = flight['Price'][0] + flight_back['Price'][0]
            return "Total coast: {} {}".format(price_sum, flight['Price'][1])
    return ''


def flybulgarien_flying(departure, arrival, departure_date, return_date=None):
    """Displays flybulgarien flights between the specified airports on the specified dates to stdout."""
    if check_arrival(departure, arrival):
        timetable_raw = get_timetable(departure, arrival, departure_date, return_date)
        timetable = timetable_parser(timetable_raw, departure, arrival, departure_date, return_date)
        timetable_printer(timetable)
    else:
        print('No flights were found between the indicated airports.')


def main():
    try:
        args = args_checker(**args_handler(**get_arg_params()))
        flybulgarien_flying(**args)
    except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
            requests.exceptions.ConnectTimeout, ServConnectionError
            ):
        print('Unable to access server.')
    except ServerError:
        print('An internal error occurred on the server.')
    except ArgumentError as err:
        print('Error in one of the attributes.')
        print(err)


if __name__ == '__main__':
    main()
